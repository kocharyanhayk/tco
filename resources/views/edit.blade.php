@extends('layouts.app')

@section('content')

    <div class="container">
        <h1><a href="{{'/task'}}">All tasks</a></h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ url('/')}}/task/{{$task->id}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label for="name">Task name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Task name" value="{{$task->name}}" required>
            </div>

            <div class="form-group">
                <label for="assigned">Select developer</label>
                <select name="assigned" class="form-control" required>
                    @foreach($developers as $developer)
                        <option @if($task->assigned_to == $developer->id) {{ "selected" }} @endif value="{{$developer->id}}">{{$developer->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="status">Task status</label>
                <input type="text" class="form-control" id="status" name="status" placeholder="Task status" value="{{\App\Helper::status($task->status)}}" disabled>
                <input type="hidden" class="form-control" name="status" placeholder="Task status" value="{{$task->status}}">
            </div>

            <div class="form-group">
                <label for="description">Task description</label>
                <textarea class="form-control" name="description" id="description" cols="30" rows="10" required>{{$task->description}}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
