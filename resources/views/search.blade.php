@extends('layouts.app')

@section('content')
    <div class="container">
        <h1><a href="{{'/task'}}">All tasks</a></h1>
        @if($search_empty == 0)
            @if(count($tasks) != 0)
                <h4 class="com_title">Results:</h4>
            @endif
            @if(!empty($tasks))
                    <ul class="list-group">
                        @foreach($tasks as $task)
                            <li class="list-group-item"><a href="{{ url('/') }}/task/{{$task->id}}">
                                    {{$task->name}}
                                </a></li>
                        @endforeach
                    </ul>
            @endif
            @if(count($tasks) == 0 )
                <div class="search_res">
                    <p>No result</p>
                </div>
            @endif
        @else
            <div class="search_res">
                <p>Please enter at least 3 symbols.</p>
            </div>
        @endif
    </div>
@endsection
