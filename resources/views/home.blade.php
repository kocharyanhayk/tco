 @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    {{ __('Dashboard') }}
                    @if(Auth::user()->status == 1)
                        <a href="{{ url('/') }}/task/create" class="float-right">+ add new task</a>
                    @endif
                    <form method="get" action="{{ asset('/') }}search">
                        {{ csrf_field() }}
                        <div class="input-group mt-4">
                            <input class="form-control my-0 py-1" type="text" name="search" placeholder="Search" aria-label="Search">
                            <button type="submit" class="btn btn-default">
                                <i class="fas fa-search" aria-hidden="true"></i>
                            </button>
                        </div>
                    </form>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(count($checked) > 0)
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Task name</th>
                                <th scope="col">Created by</th>
                                <th scope="col">Assigned to</th>
                                <th scope="col">Status</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($checked as $task)
                              <tr>
                                <th scope="row">{{$task->id}}</th>
                                <td>{{$task->name}}</td>
                                <td>{{ $task->created_task->name }}</td>
                                <td>{{ $task->asigned_task->name }}</td>
                                <td>{{\App\Helper::status($task->status)}}</td>
                                <td><p class="hide-words">{{$task->description}}</p></td>
                                <td>
                                    @if(Auth::user()->status == 1)
                                        <form class="" action="{{ url('/') }}/task/{{ $task->id }}" method="post">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                        <a class="btn btn-primary" role="button" href="{{ url('/') }}/task/{{$task->id.'/edit'}}">Edit</a>
                                        <button data-id="{{$task->id}}" type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    @else
                                        <a class="btn btn-success" role="button" href="{{ url('/') }}/task/{{$task->id}}">Details</a>
                                    @endif
                                </td>
                              </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p>You don't have created tasks yet</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
