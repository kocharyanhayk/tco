@extends('layouts.app')

@section('content')
    <div class="container">
        <h1><a href="{{'/task'}}">All tasks</a></h1>
        <ul class="list-group">
            <input type="hidden" name="task_id" id="task_id" value="{{$task->id}}">
            <li class="list-group-item"><strong>Name</strong> : <br> {{$task->name}}</li>
            <li class="list-group-item"><strong>Created by</strong> : <br> {{ $task->created_task->name }}</li>
            <li class="list-group-item"><strong>Assigned to</strong> : <br> {{ $task->asigned_task->name }}</li>
            <li class="list-group-item"><strong>Status</strong> : <br>
                <select name="status" id="status" class="form-control" @if(Auth::user()->id != $task->assigned_to) disabled @endif>
                    <option @if($task->status == '1') {{ "selected" }} @endif value="1">Created</option>
                    <option @if($task->status == '2') {{ "selected" }} @endif value="2">Assigned</option>
                    <option @if($task->status == '3') {{ "selected" }} @endif value="3">In-progress</option>
                    <option @if($task->status == '4') {{ "selected" }} @endif value="4">Done</option>
                </select>
            </li>
            <li class="list-group-item"><strong>Description</strong> : <br> {{$task->description}}</li>
        </ul>
    </div>
@endsection
