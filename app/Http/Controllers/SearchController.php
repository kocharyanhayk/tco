<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request){
        $search_word = $request->search;
        if(isset($search_word) && strlen($search_word) >= 3 ){

            $tasks = Task::select("*")
                ->where("name", 'like', "%$search_word%")
                ->orWhere("created_by", 'like', "%$search_word%")
                ->orWhere("assigned_to", 'like', "%$search_word%")
                ->orWhere("description", 'like', "%$search_word%")
                ->orderBy('created_at', 'DESC')
                ->get();
            $search_empty = 0;
            return view('search', compact('tasks','search_empty'));
        }else{
            $search_empty = 1;
            return view('search',compact('search_empty'));
        }
    }
}
