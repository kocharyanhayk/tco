<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    //
    public function status(Request $request){
        $id = $request->id;
        $status = $request->type;

        $task = Task::find($id);

        $task->status = $status;

        $task->save();

    }
}
