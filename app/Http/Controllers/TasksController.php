<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tasks = Task::where('created_by','=',Auth::user()->id)->get();
        $all = Task::all();
        $checked = (Auth::user()->status == 1) ? $tasks : $all ;

        return view('home',compact('checked'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $developers = User::where('status','=',2)->get();
        return view('create',compact('developers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'description' => ['required'],
        ]);

        $task = new Task;

        $task->name = $request->name;
        $task->created_by = Auth::user()->id;
        $task->assigned_to = $request->assigned;
        $task->description = $request->description;

        $task->save();

        $tasks = Task::where('created_by','=',Auth::user()->id)->get();
        $all = Task::all();
        $checked = (Auth::user()->id == 1) ? $tasks : $all ;

        return view('home',compact('checked'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        return view('single',compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'developers' => User::where('status','=',2)->get(),
            'task' => Task::find($id)

        ];
        return view('edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => ['required'],
//            'status' => ['required'],
            'description' => ['required'],
        ]);

        $task = Task::find($id);

        $task->name = $request->name;
        $task->created_by = Auth::user()->id;
        $task->assigned_to = $request->assigned;
        $task->status = $request->status;
        $task->description = $request->description;

        $task->save();

        $tasks = Task::where('created_by','=',Auth::user()->id)->get();
        $all = Task::all();
        $checked = (Auth::user()->id == 1) ? $tasks : $all ;

        return view('home',compact('checked'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();

        $tasks = Task::where('created_by','=',Auth::user()->id)->get();
        $all = Task::all();
        $checked = (Auth::user()->status == 1) ? $tasks : $all ;

        return view('home',compact('checked'));
    }

}
