<?php
namespace App;

class Helper {

    static function status($id){
        switch ($id) {
            case 1:
                echo 'Created';
                break;
            case 2:
                echo 'Assigned';
                break;
            case 3:
                echo 'In-progress';
                break;
            case 4:
                echo 'Done';
                break;
        }
    }

}
