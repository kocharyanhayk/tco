<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Task extends Model
{
    protected $fillable = [
        'name',
        'created_by',
        'assigned_to',
        'status',
        'description'
    ];

    public function created_task() {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function asigned_task() {
        return $this->belongsTo(User::class, 'assigned_to', 'id');
    }


}
