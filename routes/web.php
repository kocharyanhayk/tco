<?php

use App\Http\Middleware\CheckDeveloper;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'TasksController@index')->name('home');

Route::resource('/task', 'TasksController');

Route::get('/search', 'SearchController@index');
Route::post('/search', 'SearchController@index');

Route::post('/ajax', 'AjaxController@status');
